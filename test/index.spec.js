import { expect } from 'chai'
import { doSomething, doSomethingElse, readFileFromClasspath, readFileFromClasspathPromisify, readFile3 } from '../src/index.js'

describe('Testing promises', () => {
  it('should do something', () => {
    expect(doSomething()).to.be.equal('something')
  })
  it('should do something else', () => {
    expect(doSomethingElse()).to.be.equal('somethingElse')
  })
})

describe('Reading a file in Node with callbacks', () => {
  it('should read the file', (done) => {
    readFileFromClasspath('example', (error, content) => {
      if (error) {
        done(error)
      }
      expect(content).to.be.equal('Hola Borja')
      done()
    })
  })
  it('should not read the file if filename does not exist', (done) => {
    readFileFromClasspath('example2', (error, content) => {
      if (error) {
        return done()
      }
      done(Error('It has to be an previous error'))
    })
  })
})

describe('Reading a file in Node with promises', () => {
  it('should read the file', () => {
    return readFileFromClasspathPromisify('example')
      .then(content => {
        expect(content).to.be.equal('Hola Borja')
      })
  })
  it('should not read the file because ot does not exist', () => {
    return readFileFromClasspathPromisify('example2')
      .then(() => {
        throw Error('This can not happen')
      }, error => {
        expect(error).not.to.be.equal(undefined)
      })
  })
})

describe('Reading a file in Node with async await', () => {
  it('should read the file', async () => {
    const result = await readFileFromClasspathPromisify('example')
    expect(result).to.be.equal('Hola Borja')
  })
})

describe('Reading article', () => {
  it('Examples 1', () => {
    return readFileFromClasspathPromisify()
      .then(function (contentFile1) {
        expect(contentFile1).to.be.equal('Hola Borja')
        return readFile3()
      }).then(content3 => {
        expect(content3).to.be.equal('Hola Alberto')
      })
  })

  it('Examples 2', () => {
    return readFileFromClasspathPromisify()
      .then(function (contentFile1) {
        expect(contentFile1).to.be.equal('Hola Borja')
        readFile3().then(content3 => {
          expect(content3).to.be.equal('Hola Alberto')
        })
      }).then(content3 => {
        expect(content3).to.be.equal('Hola Alberto')
      })
  })

  it('Examples 3', () => {
    return readFileFromClasspathPromisify()
      .then(readFile3())
      .then(content => {
        console.log(`content: ${content}`)
        expect(content).to.be.equal('Hola Alberto')
      })
  })

  it('Examples 4', () => {
    return readFileFromClasspathPromisify()
      .then(readFile3)
      .then(content => {
        expect(content).to.be.equal('Hola Alberto')
      })
  })

  const examplePromise = Promise.resolve('Hola')

  it('Promise all example', () => {
    return Promise.all([readFileFromClasspathPromisify(), readFile3()])
      .then((contents) => {
        expect(contents[0]).to.be.equal('Hola Borja')
        expect(contents[1]).to.be.equal('Hola Alberto')
      })
  })

  it('exmple promise', () => {
    return examplePromise.then(content => expect(content).to.be.equal('Hola'))
  })
})

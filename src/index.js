import fs from 'fs'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'
import { readFile } from 'fs/promises'

const __fileName = fileURLToPath(import.meta.url)
const __dirname = dirname(__fileName)

function doSomething () {
  return 'something'
}
function doSomethingElse () {
  // ...
  return 'somethingElse'
}

function readFileFromClasspath (fileName, cb) {
  const fileNameWithExtension = `${fileName}.txt`
  const dirFile = path.join(__dirname, fileNameWithExtension)
  fs.readFile(dirFile, 'utf-8', (error, content) => {
    if (error) {
      return cb(error)
    }
    return cb(null, content)
  })
}

function readFileFromClasspathPromisify (fileName = 'example') {
  const fileNameWithExtension = `${fileName}.txt`
  const dirFile = path.join(__dirname, fileNameWithExtension)
  return readFile(dirFile, 'utf-8')
}

function readFile3 () {
  const fileNameWithExtension = 'example3.txt'
  const dirFile = path.join(__dirname, fileNameWithExtension)
  return readFile(dirFile, 'utf-8')
}

export { doSomething, doSomethingElse, readFileFromClasspath, readFileFromClasspathPromisify, readFile3 }
